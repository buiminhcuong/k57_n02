<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#login-wrapper {
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			width: 300px;
			height: 200px;
			margin: auto;
			border: solid 1px #ababab;
		}

		#login-header {
			background-color: #efefef;
		}

		@media screen and (max-width: 600px) {
			#login-wrapper {
				margin: 0;	
				width: unset;
				height: unset;
			}
		}
	</style>
</head>
<body>
	<?php 

	include '../lib/auth.php';

	if(isset($_POST["login"])) {
		$username = $_POST["username"];
		$password = $_POST["password"];

		if(!doLogin($username, $password)) {
			echo "Invalid username or password!";
		}
	}

	?>
	<div id="login-wrapper">
		<div id="login-header">Login</div>
		<form method="POST">
			<table>
				<tr>
					<td>
						Username:
					</td>
					<td>
						<input type="text" name="username">
					</td>
				</tr>
				<tr>
					<td>
						Password:
					</td>
					<td>
						<input type="Password" name="password">
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<input type="submit" name="login" value="Login">
					</td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>