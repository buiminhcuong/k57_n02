<?php include 'includes/header.php'; ?>
<a href="cat_add.php">Add</a>
<?php 
	require("../lib/controls.php");
	require_once("../lib/db.php");
	require("../lib/cat_service.php");
	
	$conn = db_connect();

	$result = getAllCat($conn);;

	printTable($result, 
		["title" => "Title", 
		"description" => "Description"],
		"cat_edit.php",
		"",
		null,
		$conf["theme"]);

	db_close($conn);
?>
<?php include 'includes/footer.php'; ?>