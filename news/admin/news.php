<?php include 'includes/header.php'; ?>
<a href="news_add.php">Add</a> <br/>
<?php 
	require("../lib/controls.php");
	require_once("../lib/db.php");
	require("../lib/cat_service.php");
	require("../lib/news_service.php");

	$conn = db_connect();
	$selected_cat = isset($_GET["cat"]) ? $_GET["cat"] : "";
?>
<form method="GET">
	<?php printCombobox(getAllCat($conn), $selected_cat, "cat", "Chọn nhóm tin"); ?>
	<input type="submit" name="filter" value="filter">
</form>
<?php
	$result = getAllNews($conn, $selected_cat);

	printTable($result, 
		["title" => "Title", 
		"summary" => "Summary",
		"content" => "Content",
		"creation_date" => "Creation Date",
		"cat_title" => "Category"],
		"news_edit.php");

	db_close($conn);
?>
<?php include 'includes/footer.php'; ?>
