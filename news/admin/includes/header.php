<!DOCTYPE html>
<html>
<head>
i	<title></title>
	<link rel="stylesheet" type="text/css" href="content/style.css">
</head>
<body>
	<div id="header">
		News management
		<div id="user-info">
			Hello, <?php
				require_once '../lib/auth.php';
				echo getLoggedInUser();
			?>
			<a href="logout.php">Logout</a>
		</div>
	</div>
	<div id="center">
		<div id="left">
			<?php include 'includes/menu.php'; ?>
		</div>
		<div id="content">
<?php 
checkLoggedIn();
?>