<?php 

function doLogin($username, $password) {
	if(isValid($username, $password)) {
		startSession();
		$_SESSION["username"] = $username;
		redirect("cat.php");
		return true;
	}	
	return false;
}

function checkLoggedIn() {
	startSession();
	if(!isset($_SESSION["username"])) {
		redirect("login.php");
	}
}

function isValid($username, $password) {
	return $username == "admin" && $password == "123";
}

function startSession() {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}
}

function redirect($page) {
	header("location:$page");
}

function getLoggedInUser(){ 
	startSession();
	return $_SESSION["username"];
}

function doLogout() {
	startSession();
	session_destroy();
	redirect("login.php");
}

?>