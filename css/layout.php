<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body {
			margin: 0;
		}
		
		#div1 {
			background-color: red;
			font-size: 20px;
			padding-left: 20px;
		}

		#div2 {
			background-color: green;
		}

		#div1, #div2 {
			display: block;
			box-sizing: border-box;
			width: 100px;
			height: 80px;
		}
	</style>
</head>
<body>
	<div id="div1">
		div1	
	</div><div id="div2">
		div2
	</div>
</body>
</html>
