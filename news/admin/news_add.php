<?php include 'includes/header.php'; ?>
<a href="cat.php">Back to list</a>
<?php 
	require("../lib/db.php");
	require("../lib/cat_service.php");
	require("../lib/news_service.php");

	if(isset($_POST["add"])) {
		$conn = db_connect();

		createNews($conn, 
			escapePostParam($conn, "title"), 
			escapePostParam($conn,"summary"),
			escapePostParam($conn,"content"),
			escapePostParam($conn,"cat"));
		
		echo("Tin '$title' thêm thành công");

		mysqli_close($conn);
	}
?>
<form method="POST">
	<table>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" required></td>
		</tr>
		<tr>
			<td>Summary</td>
			<td><textarea name="summary"></textarea></td>
		</tr>
		<tr>
			<td>Content</td>
			<td><textarea name="content"></textarea></td>
		</tr>
		<tr>
			<td>Cat</td>
			<td>
				<select name="cat">
					<option value="">Chọn nhóm tin</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="add" value="Add"></td>
		</tr>
	</table>	
</form>
<?php include 'includes/footer.php'; ?>